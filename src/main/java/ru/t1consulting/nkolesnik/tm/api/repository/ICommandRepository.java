package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
