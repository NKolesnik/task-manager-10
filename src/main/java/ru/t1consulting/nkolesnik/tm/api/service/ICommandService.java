package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
