package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void remove(Task task);

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAll();

    void clear();

}
