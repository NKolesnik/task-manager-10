package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    void remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    void clear();

}
