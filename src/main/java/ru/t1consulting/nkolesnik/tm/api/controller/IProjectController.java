package ru.t1consulting.nkolesnik.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjectList();

    void clearProjectList();

}
