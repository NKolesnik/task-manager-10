package ru.t1consulting.nkolesnik.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTaskList();

    void clearTaskList();

}
