package ru.t1consulting.nkolesnik.tm.controller;

import ru.t1consulting.nkolesnik.tm.api.controller.ITaskController;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskService;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(task);
        }
    }

    @Override
    public void clearTaskList() {
        System.out.println("[TASKS CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }


}
